# Use a smaller base image
FROM node:20-alpine3.20

# Set the working directory
WORKDIR /app

# Copy package files separately to take advantage of Docker caching
COPY package.json .
COPY package-lock.json .

# Install only production dependencies to reduce image size
RUN npm install --omit=dev

# Copy the application code
COPY . .

# Expose the port your app runs on
EXPOSE 3000

# Start the application
CMD ["npm", "start"]
